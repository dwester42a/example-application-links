package demos.servlet;

import demos.api.JiraRemoteConnectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;

@Scanned
public class DisplayOutPut extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(DisplayOutPut.class);


    private final JiraRemoteConnectionService jiraRemoteConnectionService;

    @Inject
    public DisplayOutPut( final JiraRemoteConnectionService jiraRemoteConnectionService)
    {
        this.jiraRemoteConnectionService = jiraRemoteConnectionService;
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/html");
        resp.getWriter().write("<html><head><meta name=\"decorator\" content=\"atl.general\"></head><body>");
        resp.getWriter().write("<h1>Submit a get url</h1>");
        resp.getWriter().write("<form method=\"post\" target=\"other\">");
        resp.getWriter().write("URL: <input type=\"text\" name=\"url\"/>");
        resp.getWriter().write("<input type=\"submit\" value=\"send\"/>");
        resp.getWriter().write("</form>");

        resp.getWriter().write("</body></html>");
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/plain");
        resp.getWriter().write(this.jiraRemoteConnectionService.getContent(req.getParameter("url")));
    }
}