package demos.api;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.net.*;
import demos.servlet.DisplayOutPut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by daniel on 1/30/18.
 */
@Component
public class JiraRemoteConnectionService
{
    @ComponentImport
    private final ApplicationLinkService applicationLinkService;


    @Inject
    public JiraRemoteConnectionService( final ApplicationLinkService applicationLinkService)
    {
        this.applicationLinkService = applicationLinkService;
    }

    public String getContent(final String url)
    {
        final StringBuffer stringBuffer = new StringBuffer();
        for(final ApplicationLink applicationLink: this.applicationLinkService.getApplicationLinks(JiraApplicationType.class))
        {

            try
            {

                Request request = applicationLink.createImpersonatingAuthenticatedRequestFactory().createRequest(Request.MethodType.GET, url);




                 request.executeAndReturn(new ReturningResponseHandler() {
                    public Object handle(Response response) throws ResponseException
                    {
                        if( response.isSuccessful())
                        {
                            stringBuffer.append( applicationLink.getName());
                            stringBuffer.append("\n");
                            stringBuffer.append(url);
                            stringBuffer.append("\n");
                            stringBuffer.append( response.getResponseBodyAsString());
                            stringBuffer.append("\n\n");


                        }
                        else
                        {
                            stringBuffer.append( response.getStatusCode());
                        }
                        return null;
                    }
                });

            }
            catch(Exception e)
            {
                stringBuffer.append("Error received\n");
            }

        }
        return stringBuffer.toString();
    }
}
